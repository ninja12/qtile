# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Key, Group, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

##### Key bindings #####

mod = "mod4"
terminal = guess_terminal()


keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    Key([mod, "shift"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn("alacritty"), desc="Launch terminal"),

    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    Key([mod], "f", lazy.spawn("firefox"), desc="Launch web browser"),
    Key([mod], "v", lazy.spawn("virt-manager"), desc="Launch hypervisor"),
    Key([mod], "e", lazy.spawn("microsoft-edge-dev"), desc="Launch ms edge"),
    Key([mod], "n", lazy.spawn("nitrogen"), desc="Launch wallpaper manager"),
    
    ]


##### Groups #####

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

##### Layouts #####
layouts = [
    #layout.Columns(border_focus_stack='#d75f5f'),
    #layout.Max(),
    #layout.Stack(num_stacks=2),
    #layout.Bsp(),
    layout.MonadTall(),
    layout.TreeTab(),
    layout.Floating(),
   # layout.Matrix(),
   # layout.MonadWide(),
   # layout.RatioTile(),
   # layout.Tile(),
   # layout.VerticalTile(),
   # layout.Zoomy(),
]



##### COLORS ######
color = ["#ff79c6", #pink
         "#00d787", #green
         "#72003e", #wine
         "#af005f", #magenta
         "#00afd7", #cyan
         "#711c91", #purple
         "#BF46B3", #dark pink
         "#006600", #dark green
         ]


##### WIDGETS #####
widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

net = widget.Net(
   foreground = color[0], #acqua.final
   interface="enp0s31f6",
   ) 

chord =  widget.Chord(
   chords_colors={
   'launch': ("#ff0000", "#ffffff"),
    },
   name_transform=lambda name: name.upper(),
   )

cpu = widget.CPU(
   foreground = color[0], 
   )

memory = widget.Memory(
   foreground = color[1], #acqua.final
   ) 

groupbox = widget.GroupBox(
    rounded = True,
    disable_drag = True,
    highlight_method = "border",
   # active = color[4],
   # foreground = color[0],
   # highlight_color = color[3],
    other_current_screen_border = color[1],
    other_screen_border = color[7],
    this_current_screen_border = color[0],
    this_screen_border = color[2],
        )

prompt = widget.Prompt()
window_name = widget.WindowName()
systray = widget.Systray()
clock = widget.Clock(foreground = color[1], format='%Y-%m-%d %a %I:%M %p')
quickexit = widget.QuickExit()
sep = widget.Sep(foreground = color[2], linewidth = 3, padding = 10)
sep_no_bar = widget.Sep(padding = 5, linewidth = 0)

##### SCREENS #####
screens = [
    Screen(top=bar.Bar([sep_no_bar, groupbox, prompt, window_name, chord, cpu, sep, memory, sep, net, sep, systray, clock], 24,),),
    Screen(top=bar.Bar([sep_no_bar, groupbox, prompt, window_name, chord, cpu, sep, memory, sep, net, sep, systray, clock], 24,),),
]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
